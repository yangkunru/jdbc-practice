package db_connect;

import model.Student;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class StudentRepo {
    private DbConnector dbConnector;
    
    public StudentRepo() {
        dbConnector = new DbConnector();
    }
    
    
    public boolean addStudent(Student student) {
        // Need to be implemented
        if(student == null){
            throw new NullPointerException();
        }
        String code = "INSERT INTO student (name, age, gender, phone) VALUES (" + student.getName() + "," + String.valueOf(student.getAge()) + "," + student.getGender() + "," + student.getPhone() + ")";
        try(Connection connection = dbConnector.createConnect();
            Statement statement = connection.createStatement()){
            return statement.executeUpdate(code) > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
    
    public List<Student> findStudents() {
        // Need to be implemented

        String code = "SELECT id, name, age, age, gender, phone FROM student";
        List<Student> students = new ArrayList<Student>();
        try(Connection connection = dbConnector.createConnect();
            Statement statement = connection.createStatement();) {
            ResultSet resultSet = statement.executeQuery(code);
            while(resultSet.next()) {
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                int age = resultSet.getInt("age");
                String gender = resultSet.getString("gender");
                String phone = resultSet.getString("phone");
                Student student = new Student(id,name,age,gender,phone);
                students.add(student);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return students;
    }
    
    public boolean updateStudent(Student student) {
        // Need to be implemented
        if(student == null){
            throw new NullPointerException();
        }
        String code = String.format("UPDATE student SET name = '%s', age = %d, gender = '%s', phone = '%s' WHERE id = %d", student.getName(), student.getAge(), student.getGender(), student.getPhone(), student.getId());
        try(Connection connection = dbConnector.createConnect();
            Statement statement = connection.createStatement()){
            return statement.executeUpdate(code) > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
    
    public boolean deleteStudent(Student student) {
        // Need to be implemented
        if(student == null){
            throw new NullPointerException();
        }
        String code = String.format("DELETE FROM student WHERE id = %d", student.getId());
        try(Connection connection = dbConnector.createConnect();
            Statement statement = connection.createStatement()){
            return statement.executeUpdate(code) > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}
